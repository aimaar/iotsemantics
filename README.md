This repository contains software and example data in different RDF formats, as well as ontology and rules used in traffic reasoning experiments.
Semantic Reasoning for Context-aware Internet of Things Applications. Maarala AI, Su X, Riekki J
(2016), in IEEE Internet of Things Journal, DOI: 10.1109/JIOT.2016.2587060.

This project is licensed under the terms of the MIT license.
